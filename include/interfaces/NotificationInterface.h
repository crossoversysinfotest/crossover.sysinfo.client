/*
 * NotificationInterface.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_NOTIFICATIONINTERFACE_H_
#define INCLUDE_NOTIFICATIONINTERFACE_H_
#include <string>

using namespace std;

class NotificationInterface {
public:
  /*NotificationInterface();
  virtual ~NotificationInterface();*/

  virtual void send(char * s) = 0;
};

#endif /* INCLUDE_NOTIFICATIONINTERFACE_H_ */
