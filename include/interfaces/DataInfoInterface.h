/*
 * DataInfoInterface.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_DATAINFOINTERFACE_H_
#define INCLUDE_DATAINFOINTERFACE_H_

class DataInfoInterface {
public:

  virtual void update() = 0;
  virtual int get() = 0;
};

#endif /* INCLUDE_DATAINFOINTERFACE_H_ */
