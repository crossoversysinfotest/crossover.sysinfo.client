/*
 * Client.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/DataInfoInterface.h"
#include "include/interfaces/NotificationInterface.h"
#include "include/modules/LoadConfig.h"

#ifndef INCLUDE_CLIENT_H_
#define INCLUDE_CLIENT_H_

class Client {
public:
  Client();
  ~Client();
  void run();

private:
  DataInfoInterface * memory_free_;
  DataInfoInterface * cpu_usage_;
  DataInfoInterface * cpu_temperature_;
  DataInfoInterface * process_count_;
  NotificationInterface * notification;
  LoadConfig * config_;
};

#endif /* INCLUDE_CLIENT_H_ */
