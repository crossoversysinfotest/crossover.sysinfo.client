/*
 * LoadConfig.h
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#ifndef INCLUDE_MODULES_LOADCONFIG_H_
#define INCLUDE_MODULES_LOADCONFIG_H_

class LoadConfig {
public:
  LoadConfig();
  ~LoadConfig();
  int get_port();
  int get_timeout();
  int get_id();
private:
  int port_;
  int timeout_;
  int id_;
};

#endif /* INCLUDE_MODULES_LOADCONFIG_H_ */
