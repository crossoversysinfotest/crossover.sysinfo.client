/*
 * MemoryFree.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/DataInfoInterface.h"

#ifndef INCLUDE_MEMORYFREE_H_
#define INCLUDE_MEMORYFREE_H_

class MemoryFree : public DataInfoInterface {
public:
  MemoryFree();
  ~MemoryFree();
  void update();
  int get();

private:
  int value_;
};

#endif /* INCLUDE_MEMORYFREE_H_ */
