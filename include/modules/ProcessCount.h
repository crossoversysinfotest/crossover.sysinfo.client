/*
 * ProcessCount.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/DataInfoInterface.h"

#ifndef INCLUDE_PROCESSCOUNT_H_
#define INCLUDE_PROCESSCOUNT_H_

class ProcessCount : public DataInfoInterface {
public:
  ProcessCount();
  ~ProcessCount();
  void update();
  int get();

private:
  int value_;
};

#endif /* INCLUDE_PROCESSCOUNT_H_ */
