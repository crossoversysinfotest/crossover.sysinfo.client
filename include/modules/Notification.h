/*
 * Notification.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/NotificationInterface.h"
#include <string>
#include <zmq.hpp>

using namespace std;

#ifndef INCLUDE_NOTIFICATION_H_
#define INCLUDE_NOTIFICATION_H_

class Notification : public NotificationInterface {
public:
  Notification();
  Notification(int port);
  ~Notification();
  void send(char * s);

private:
  void Configure();
  int port_;
  zmq::context_t * context_;
  zmq::socket_t * publisher_;
};

#endif /* INCLUDE_NOTIFICATION_H_ */
