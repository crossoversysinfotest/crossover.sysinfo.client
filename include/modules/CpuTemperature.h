/*
 * CpuTemperature.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/DataInfoInterface.h"

#ifndef INCLUDE_CPUTEMPERATURE_H_
#define INCLUDE_CPUTEMPERATURE_H_

class CpuTemperature : public DataInfoInterface {
public:
  CpuTemperature();
  ~CpuTemperature();
  void update();
  int get();

private:
  int value_;
};

#endif /* INCLUDE_CPUTEMPERATURE_H_ */
