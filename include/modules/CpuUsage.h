/*
 * CpuUsage.h
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include "include/interfaces/DataInfoInterface.h"

#ifndef INCLUDE_CPUUSAGE_H_
#define INCLUDE_CPUUSAGE_H_

class CpuUsage : public DataInfoInterface {
public:
  CpuUsage();
  ~CpuUsage();
  void update();
  int get();

private:
  int value_;
};

#endif /* INCLUDE_CPUUSAGE_H_ */
