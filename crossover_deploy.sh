#!/bin/bash 

DEPLOY_FOLDER="../../deploy/client"

if [ ! -d ${DEPLOY_FOLDER} ]
then
  mkdir ${DEPLOY_FOLDER}
fi

echo "Copying files to ${DEPLOY_FOLDER}"
cp -v bin/client ${DEPLOY_FOLDER}
cp -v etc/* ${DEPLOY_FOLDER}
echo "Finished"

