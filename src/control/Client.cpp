/*
 * Client.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */
#include <iostream>
#include <unistd.h>
#include "include/control/Client.h"

#include "../../include/modules/CpuUsage.h"
#include "../../include/modules/MemoryFree.h"
#include "include/modules/CpuTemperature.h"
#include "include/modules/ProcessCount.h"
#include "include/modules/Notification.h"
#include "../../include/modules/Definitions.h"

Client::Client() {
  memory_free_ = new MemoryFree();
  cpu_usage_ = new CpuUsage();
  cpu_temperature_ = new CpuTemperature();
  process_count_ = new ProcessCount();
  config_ = new LoadConfig();
  notification = new Notification(config_->get_port());
}

Client::~Client() {
  delete memory_free_;
  delete cpu_usage_;
  delete cpu_temperature_;
  delete process_count_;
  delete notification;
  delete config_;
}

void Client::run() {
  while(1) {
    memory_free_->update();
    cpu_usage_->update();
    cpu_temperature_->update();
    process_count_->update();

    std::cout << "\nClient running:\n";
    std::cout << " Memory Free:     " << memory_free_->get()     << " KB"   << std::endl;
    std::cout << " Cpu usage:       " << cpu_usage_->get()       << " %"    << std::endl;
    std::cout << " Cpu temperature: " << cpu_temperature_->get() << " ºC"    << std::endl;
    std::cout << " Process count:   " << process_count_->get()   << " processes" << std::endl;

    char message[MAX_MESSAGE_SIZE] = {0};
    sprintf(message, "%d %d %d %d %d",
               config_->get_id(),
               memory_free_->get(),
               cpu_usage_->get(),
               cpu_temperature_->get(),
               process_count_->get());

    notification->send((char*)message);

    sleep(config_->get_timeout());
  }
}

