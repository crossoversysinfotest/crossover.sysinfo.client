 
#include <iostream>

#include "include/control/Client.h"

using namespace std;

int main()
{
  std::cout << "Client Monitor Application!" << std::endl;
  Client client;

  client.run();

  return 0;
}

