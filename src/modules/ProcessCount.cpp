/*
 * ProcessCount.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#include "include/modules/ProcessCount.h"
#include "sys/sysinfo.h"

ProcessCount::ProcessCount() {
  value_ = 0;
}

ProcessCount::~ProcessCount() {
}

void ProcessCount::update() {
  struct sysinfo memInfo;
  sysinfo (&memInfo);
  value_ = memInfo.procs;
}

int ProcessCount::get() {
  return value_;
}
