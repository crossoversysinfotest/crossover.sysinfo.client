/*
 * CpuUsage.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#include "../../include/modules/CpuUsage.h"
#include "include/modules/Definitions.h"
#include "sys/sysinfo.h"


CpuUsage::CpuUsage() {
  value_ = 0;
}

CpuUsage::~CpuUsage() {
}

/**
 * Update CPU usage using sysinfo call.
 * The "loads" is a 3 elements size:
 *  [0] = 1 min average cpu load
 *  [1] = 5 min average cpu load
 *  [2] = 15 min average cpu load
 */
void CpuUsage::update() {
  struct sysinfo memInfo;
  sysinfo (&memInfo);
  value_ = memInfo.loads[1]/CONVERT_TO_PERCENTAGE; //5 min average
}

int CpuUsage::get() {
  return value_;
}

