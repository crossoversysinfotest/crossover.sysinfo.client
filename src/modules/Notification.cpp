/*
 * Notification.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#include "include/modules/Notification.h"
#include "include/modules/Definitions.h"
#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

Notification::Notification() {
  port_ = DEFAULT_PORT;
  Configure();
}

Notification::Notification(int port) {
  port_ = port;
  Configure();
}

void Notification::Configure() {
  //  Prepare our context and publisher
  context_ = new zmq::context_t(1);
  publisher_ = new zmq::socket_t(*context_, ZMQ_PUB);
  char connection[50] = {0};
  sprintf(connection, "tcp://*:%d", port_);
  publisher_->bind(connection);
  publisher_->bind("ipc://weather.ipc");
}

Notification::~Notification() {
  delete publisher_;
  delete context_;
}

void Notification::send(char * s) {
  //  Send message to all subscribers
  zmq::message_t message(MAX_MESSAGE_SIZE);
  snprintf ((char *)message.data(), MAX_MESSAGE_SIZE,  "%s", s);
  publisher_->send(message);
  cout << "Message sent: " << s << endl;
}

