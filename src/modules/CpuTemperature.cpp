/*
 * CpuTemperature.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#include "include/modules/CpuTemperature.h"
#include "include/modules/Definitions.h"
#include <stdio.h>


CpuTemperature::CpuTemperature() {
  value_ = 0;
}

CpuTemperature::~CpuTemperature() {
}

/**
 * Update currenv value by reading from cpu thermal zone
 */
void CpuTemperature::update() {
  int temperature = 0;
  FILE* file;
  file = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
  fscanf(file, "%d", &temperature);
  fclose(file);
  value_ = temperature/TEMP_DIVISOR;

}

int CpuTemperature::get() {
  return value_;
}
