/*
 * LoadConfig.cpp
 *
 *  Created on: 28 de out de 2017
 *      Author: danilo
 */

#include "include/modules/LoadConfig.h"
#include "include/modules/Definitions.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

LoadConfig::LoadConfig() {
  timeout_ = 0;
  port_ = 0;
  id_ = 0;
  string line;
  ifstream file(CONFIG_FILE_NAME);
  if (file.is_open()) {
    cout << "Configuration loaded from " << CONFIG_FILE_NAME << endl;
    file >> line;
    file >> id_;
    cout << line << " " << id_  << '\n';
    file >> line;
    file >> port_;
    cout << line << " " << port_  << '\n';
    file >> line;
    file >> timeout_;
    cout << line << " " << timeout_  << '\n';
    file.close();
  }
  else
  {
    cout << "Unable to open configuration file. Using Default values.";
    id_ = DEFAULT_ID;
    port_ = DEFAULT_PORT;
    timeout_ = DEFAULT_TIMEOUT;
  }
}

LoadConfig::~LoadConfig() {
}

int LoadConfig::get_id(){
  return id_;
}

int LoadConfig::get_port(){
  return port_;
}

int LoadConfig::get_timeout(){
  return timeout_;
}
