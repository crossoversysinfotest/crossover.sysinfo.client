/*
 * MemoryFree.cpp
 *
 *  Created on: 27 de out de 2017
 *      Author: danilo
 */

#include "../../include/modules/MemoryFree.h"
#include "include/modules/Definitions.h"
#include "sys/sysinfo.h"


MemoryFree::MemoryFree() {
  value_ = 0;
}

MemoryFree::~MemoryFree() {
}

void MemoryFree::update() {
  struct sysinfo memInfo;
  sysinfo (&memInfo);
  value_ = memInfo.freeram*memInfo.mem_unit/CONVERT_TO_KB;
}

int MemoryFree::get() {
  return value_;
}
