#!/bin/bash

echo "BUILDING CLIENT..."
if [ ! -d build ]
then
  mkdir build
fi
cd build
cmake ..
make
cd -
